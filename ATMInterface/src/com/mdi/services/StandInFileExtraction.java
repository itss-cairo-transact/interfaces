/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-18 14:30:04
 * @modify date 2022-04-18 14:30:04
 * @desc [description]
 */

package com.mdi.services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.temenos.api.TField;
import com.temenos.api.exceptions.T24CoreException;
import com.temenos.t24.api.complex.ac.accountapi.Balance;
import com.temenos.t24.api.complex.eb.servicehook.ServiceData;
import com.temenos.t24.api.hook.system.ServiceLifecycle;
import com.temenos.t24.api.party.Account;
import com.temenos.t24.api.records.account.AccountRecord;
import com.temenos.t24.api.records.aclockedevents.AcLockedEventsRecord;
import com.temenos.t24.api.records.atmparameter.AtmParameterRecord;
import com.temenos.t24.api.records.company.CompanyRecord;
import com.temenos.t24.api.records.currency.CurrencyRecord;
import com.temenos.t24.api.records.customer.CustomerRecord;
import com.temenos.t24.api.records.limit.LimitRecord;
import com.temenos.t24.api.records.postingrestrict.PostingRestrictRecord;
import com.temenos.t24.api.records.stcdmmonitor.StCdmMonitorRecord;
import com.temenos.t24.api.system.DataAccess;

public class StandInFileExtraction extends ServiceLifecycle {
    // DataAccess Object
    DataAccess dataAccess = new DataAccess(this);
    static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    static LocalDateTime now = LocalDateTime.now();
    static String today = dtf.format(now);

    @Override
    public List<String> getIds(ServiceData serviceData, List<String> controlList) {
        List<String> fileNames = new ArrayList<String>();
        try {
            // Get System record data
            AtmParameterRecord atmParameter = new AtmParameterRecord(dataAccess.getRecord("ATM.PARAMETER", "SYSTEM"));
            // Get Out Dir Path from local ref field
            String dirOutPath = atmParameter.getLocalRefGroups("L.BATCH.OUT.DIR").get(0)
                    .getLocalRefField("L.BATCH.OUT.DIR").getValue().replace("\\", "/") + "/";

            // Get IN Dir Path from local ref field
            String dirInPath = atmParameter.getLocalRefGroups("L.BATCH.IN.DIR").get(0)
                    .getLocalRefField("L.BATCH.IN.DIR").getValue().replace("\\", "/") + "/";

            // Read files name
            File[] files = new File(dirInPath).listFiles();
            for (File file : files) {
                if (file.getName().endsWith(".txt"))
                    fileNames.add(new String().join(",", dirInPath, dirOutPath, file.getName()));
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return fileNames;
    }

    @Override
    public void process(String id, ServiceData serviceData, String controlItem) {
        String[] fileObj = id.split(",");
        String inPath = fileObj[0];
        String outPath = fileObj[1];
        String fileName = fileObj[2];
        Double sumAvailBal = 0D;
        Integer numOfRec = 0;
        StringBuilder fileOutText = new StringBuilder();
        File fileData = new File(inPath + fileName);
        try (Scanner myReader = new Scanner(fileData)) {
            while (myReader.hasNextLine()) {
                // Holds current line data which's ID
                String singleID = myReader.nextLine();
                // Holds account record of the current id
                try {
                    AccountRecord accountRec = new AccountRecord(dataAccess.getRecord("ACCOUNT", singleID.trim()));
                    // Holds currency record of the current account id -->
                    // currency field value
                    if (!accountRec.equals(null)) {
                        CompanyRecord acctCompany = new CompanyRecord(
                                dataAccess.getRecord("COMPANY", accountRec.getCoCode()));
                        CustomerRecord customer = new CustomerRecord(
                                dataAccess.getRecord("CUSTOMER", accountRec.getCustomer().getValue()));
                        CurrencyRecord currency = new CurrencyRecord(
                                dataAccess.getRecord("CURRENCY", accountRec.getCurrency().getValue()));

                        Double availBal = 0D, limitAmt = 0D;

                        // calculate total locked amount
                        List<String> lockedEvtIds = new ArrayList<String>();
                        lockedEvtIds = dataAccess.selectRecords(acctCompany.getMnemonic().getValue(),
                                "AC.LOCKED.EVENTS", "", "WITH ACCOUNT.NUMBER EQ '" + singleID.trim() + "'");
                        Double totalLockedAmt = 0D;
                        for (String lckEvtId : lockedEvtIds) {
                            AcLockedEventsRecord acLockedEvt = new AcLockedEventsRecord(dataAccess
                                    .getRecord(acctCompany.getMnemonic().getValue(), "AC.LOCKED.EVENTS", "", lckEvtId));
                            totalLockedAmt += new Double(acLockedEvt.getLockedAmount().getValue());
                        }

                        try {
                            Account account = new Account(this);
                            account.setAccountId(singleID.trim());
                            Balance bal = account.getAvailableBalance("AVAILABLE");
                            // limitAmt =
                            // Double.parseDouble(bal.getAmount().getValue().toString())
                            // -
                            // Double.parseDouble(accountRec.getWorkingBalance().getValue())
                            // + totalLockedAmt;
                            availBal += bal.getAmount().getValue().doubleValue();
                        } catch (T24CoreException e) {

                        }
                        availBal += totalLockedAmt;
                        sumAvailBal += Math.abs(availBal);

                        String postingRestrict = "0";
                        List<TField> custPostRstrct = customer.getPostingRestrict();
                        List<TField> acctPostsRstrct = accountRec.getPostingRestrict();
                        Map<String, String> postRstrctMap = new HashMap<String, String>();

                        if (!custPostRstrct.isEmpty()) {
                            for (TField custPR : custPostRstrct) {
                                PostingRestrictRecord pr = new PostingRestrictRecord(
                                        dataAccess.getRecord("POSTING.RESTRICT", custPR.getValue()));
                                postRstrctMap.put(pr.getRestrictionType().getValue(), "POSTING-RESTRICTION");
                            }
                        }
                        if (!acctPostsRstrct.isEmpty()) {
                            for (TField acctPR : acctPostsRstrct) {
                                PostingRestrictRecord pr = new PostingRestrictRecord(
                                        dataAccess.getRecord("POSTING.RESTRICT", acctPR.getValue()));
                                postRstrctMap.put(pr.getRestrictionType().getValue(), "POSTING-RESTRICTION");
                            }
                        }

                        // IF CUSTOMER OR ACCOUNT
                        if (!postRstrctMap.isEmpty()) {
                            if (postRstrctMap.keySet().size() == 1) {
                                if (postRstrctMap.containsKey("DEBIT")) {
                                    postingRestrict = "1";
                                } else if (postRstrctMap.containsKey("CREDIT")) {
                                    postingRestrict = "2";
                                } else {
                                    postingRestrict = "3";
                                }
                            } else {
                                postingRestrict = "3";
                            }
                        }

                        boolean isCustomerDormant = false;
                        StCdmMonitorRecord stCdmMonitorRec = null;
                        try {
                            stCdmMonitorRec = new StCdmMonitorRecord(
                                    dataAccess.getRecord("ST.CDM.MONITOR", accountRec.getCustomer().getValue()));
                            if (!stCdmMonitorRec.equals(null)) {
                                isCustomerDormant = true;
                            }
                        } catch (T24CoreException e) {
                            // System.out.println(
                            // "ATM-STANDIN Service: EXCEPTTION - ST.CDM.MONITOR
                            // NOT FOUND! --> Customer: "
                            // + accountRec.getCustomer().getValue());
                        }

                        String dormant = "0";
                        if (!accountRec.getInactivMarker().getValue().isEmpty()
                                && accountRec.getInactivMarker().getValue().equals("1")) {
                            dormant = "1";
                        }
                        if (isCustomerDormant) {
                            dormant = "2";
                        }
                        if (!accountRec.getInactivMarker().getValue().isEmpty()
                                && accountRec.getInactivMarker().getValue().equals("1") && isCustomerDormant) {
                            dormant = "3";
                        }
                        // Holds data of each record to be writen to file

                        String lineData = prepareData("MDI ", 4, false) + // instcode
                                prepareData(accountRec.getCoCode(), 8, false) + // brncode
                                // prepareData(singleID, 28, false) + // accno
                                singleID + "|" + // accno
                                prepareData(currency.getNumericCcyCode().getValue(), 3, false) + // currcode
                                prepareData(postingRestrict + dormant, 2, false) + // statcode
                                prepareData(totalLockedAmt.toString(), 20, true) + // blkamt
                                prepareData(availBal.toString(), 20, true) + // avlbal
                                prepareData(accountRec.getOnlineClearedBal().getValue(), 20, true) + // clrbal
                                prepareData(accountRec.getOnlineActualBal().getValue(), 20, true) + // unclrbal
                                prepareData(limitAmt.toString(), 20, true); // credit_limit
                        // Write data to the extracted file
                        // writeToFile(fileName.replace(".txt", ""), outPath,
                        // lineData);
                        fileOutText.append(lineData + "\n");
                        numOfRec += 1;
                    }
                } catch (T24CoreException e) {
                    continue;
                }
            }
            writeToFile(fileName.replace(".txt", ""), outPath, fileOutText,
                    prepareData(sumAvailBal.toString(), 18, true), numOfRec);
            // Close the file when there's no more data to read
            myReader.close();
            // Delete File when processing is done

            File fileBkp = new File(inPath + fileName + ".bkp-" + today);
            fileData.renameTo(fileBkp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeToFile(String filename, String path, StringBuilder text, String sumAvailBal,
            Integer numOfRec) {
        try {
            String header = "HEADERBALANCE" + String.format("%-12s", "T24") + String.format("%-3s", "") + today + "\n";
            String trailer = "TRAILER" + today.substring(0, 8) + String.format("%8s", numOfRec).replace(" ", "0")
                    + sumAvailBal + "0";
            FileWriter myWriter = new FileWriter(path + filename + "-Extracted.txt", true);
            myWriter.write(header + text.toString() + trailer.replace("|", ""));
            myWriter.close();
        } catch (IOException e) {
            e.getMessage();
        }
    }

    private static String prepareData(String text, int length, boolean isAmt) {
        if (isAmt) {
            text = new DecimalFormat("0.00").format(Double.parseDouble(text.equals("") ? "0" : text));
        }
        String zeros = "";
        for (int i = text.length(); i < length; i++) {
            zeros += "0";
        }
        // return zeros + text + "|";
        return (text.indexOf("-") != -1) ? text.replace("-", "-" + zeros) + "|" : zeros + text + "|";
    }
}
