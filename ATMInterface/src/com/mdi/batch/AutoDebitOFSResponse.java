/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-18 14:29:44
 * @modify date 2022-04-18 14:29:44
 * @desc [description]
 */

package com.mdi.batch;

import com.temenos.api.TString;
import com.temenos.t24.api.complex.ft.clearinghook.ClearingContext;
import com.temenos.t24.api.hook.clearing.InwardEntry;
import com.temenos.t24.api.records.acinwardentry.AcInwardEntryRecord;

public class AutoDebitOFSResponse extends InwardEntry {

    @Override
    public void modifyLocalReferenceValues(String inwardEntryId, String ofsClearingMessageType,
            ClearingContext clearingContext, AcInwardEntryRecord clearingRecord, TString clearingResponse) {

        clearingResponse.set("\\"+clearingRecord.getAmountLcy().getValue());
    }
}
