/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-18 14:28:49
 * @modify date 2022-04-18 14:28:49
 * @desc [description]
 */

package com.mdi.interfaces;

import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.dates.DatesRecord;
import com.temenos.t24.api.system.DataAccess;

public class ATMResponseUpdateDE17 extends AtmMessageLifecycle{
    @Override
    public String getFieldValue(String isoRequestFieldValue, AtmTransactionContext atmTransactionContext,
            IsoRequest isoRequest, String intrfaceMessageId) {
        DataAccess dataAccess = new DataAccess(this); 
        DatesRecord t24Date = new DatesRecord(dataAccess.getRecord("DATES", "EG0010001"));          
        return t24Date.getToday().getValue().substring(4,8); // MM/DD
    }
}
