/**
 * @author Amr Alargawy
 * @email aalargawy@itssglobal.com
 * @create date 2022-04-19 16:57:59
 * @modify date 2022-04-19 16:57:59
 * @desc [description]
 */

package com.mdi.interfaces;

import java.util.List;

import com.ibm.icu.text.DecimalFormat;
import com.temenos.api.exceptions.T24CoreException;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.aclockedevents.AcLockedEventsRecord;
import com.temenos.t24.api.records.company.CompanyRecord;
import com.temenos.t24.api.system.DataAccess;

public class ATMRequestAdjIncrementalAuthAmt extends AtmMessageLifecycle {

    @Override
    public String getFieldValue(String isoRequestFieldValue, AtmTransactionContext atmTransactionContext,
            IsoRequest isoRequest, String intrfaceMessageId) {

        Double incrementalAmt = new Double(isoRequestFieldValue) / 100;
        DataAccess dataAccess = new DataAccess(this);
        // get company record
        CompanyRecord company = new CompanyRecord(dataAccess.getRecord("COMPANY", "EG0011001"));
        // build the reservation key 37*102 to open AC.LOCKED.EVENTS records
        String reservationKey = isoRequest.getRetrievalReferenceNumber() + isoRequest.getAccountNumber();
        try {
            // get AC.LOCKED.EVENTS record of original transaction
            List<String> acLkdList = dataAccess.selectRecords(company.getMnemonic().getValue(), "AC.LOCKED.EVENTS", "",
                    "WITH RESERVE.ALT.KEY LIKE '" + reservationKey.trim() + "...'");
            for (String recId : acLkdList) {
                AcLockedEventsRecord acLockedEvt = new AcLockedEventsRecord(
                        dataAccess.getRecord(company.getMnemonic().getValue(), "AC.LOCKED.EVENTS", "", recId));
                // get orginal locked amount
                Double originalTxnAmt = new Double(acLockedEvt.getLockedAmount().getValue());
                incrementalAmt += originalTxnAmt;
            }
        } catch (T24CoreException e) {
            // e.printStackTrace();
        }

        return new DecimalFormat("0.00").format(incrementalAmt).toString();
    }
}
