/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-18 14:28:29
 * @modify date 2022-04-19 13:34:21
 * @desc [description]
 */

package com.mdi.interfaces;

import com.temenos.api.TString;
import com.temenos.api.TStructure;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequestHeader;
import com.temenos.t24.api.complex.eb.servicehook.SynchronousTransactionData;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.account.AccountRecord;
import com.temenos.t24.api.records.dates.DatesRecord;
import com.temenos.t24.api.system.DataAccess;

public class ATMResponseSetBalanceInquryDate extends AtmMessageLifecycle{

    @Override
    public void updateRecord(AtmTransactionContext atmTransactionContext, IsoRequest isoRequest,
            IsoRequestHeader isoRequestHeader, TString ofsResponseMessage, SynchronousTransactionData transactionData,
            TStructure record) {

        DataAccess dataAccess = new DataAccess(this); 
        // Holding Current company mnemonic
        String companyMnemonic = atmTransactionContext.getCompanyMnemonic(); 
        
        // Get account record by ISO request DE 102 (Account Number)
        AccountRecord accountRecord = new AccountRecord(
            dataAccess.getRecord(companyMnemonic, "ACCOUNT", "", isoRequest.getAccountNumber())
        );

        // Get Dates record data
        DatesRecord t24Date = new DatesRecord(dataAccess.getRecord("DATES","EG0010001"));
        // Update Account record Local Field L.LAST.LOGIN.DATE
        accountRecord.getLocalRefField("L.LAST.LOGIN.DATE").set(t24Date.getToday().getValue());

        // Set account record TStructer into record param
        record.set(accountRecord.toStructure());

        dataAccess.updateLocalfields("ACCOUNT", isoRequest.getAccountNumber(), record) ;     
    }

}
