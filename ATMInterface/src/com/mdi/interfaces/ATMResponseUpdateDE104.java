/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-18 14:28:38
 * @modify date 2022-04-18 14:28:38
 * @desc [description]
 */

package com.mdi.interfaces;

import com.temenos.api.exceptions.T24CoreException;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequestHeader;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.atmtransaction.AtmTransactionRecord;
import com.temenos.t24.api.system.DataAccess;

public class ATMResponseUpdateDE104 extends AtmMessageLifecycle {

    @Override
    public String updateOfsResponseMessage(AtmTransactionContext atmTransactionContext, IsoRequest isoRequest,
            IsoRequestHeader isoRequestHeader, String ofsResponseMessage) {
        DataAccess dataAccess = new DataAccess(this);
        // OFSRESPONSEMESSAGE = OFSRESPONSEMESSAGE:','REF.TXN:1:1=AtTransId
        // ATMFRM.Foundation.getAtUniqueId()
        // 0*3*13*37*102
        String uniqueID = isoRequest.getMessageTypeIndicator() + isoRequest.getProcessingCode()
                + isoRequest.getLocalTransactionDate() + isoRequest.getRetrievalReferenceNumber()
                + isoRequest.getAccountNumber();
        String acInwardEntryRef = "";
        try {
            AtmTransactionRecord atmTransactionRecord = new AtmTransactionRecord(
                    dataAccess.getRecord("ATM.TRANSACTION", uniqueID.trim()));
            acInwardEntryRef += atmTransactionRecord.getTransRef(0).getValue();
        } catch (T24CoreException e) {

        }

        return ofsResponseMessage + ",REF.TXN:1:1=" + acInwardEntryRef;
    }
}