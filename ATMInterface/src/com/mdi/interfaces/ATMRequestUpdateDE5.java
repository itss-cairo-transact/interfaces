package com.mdi.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.temenos.api.exceptions.T24CoreException;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.account.AccountRecord;
import com.temenos.t24.api.records.aclockedevents.AcLockedEventsRecord;
import com.temenos.t24.api.records.company.CompanyRecord;
import com.temenos.t24.api.records.currency.CurrencyRecord;
import com.temenos.t24.api.records.dates.DatesRecord;
import com.temenos.t24.api.records.limit.LimitRecord;
import com.temenos.t24.api.system.DataAccess;

/**
 * TODO: Document me!
 *
 * @author alargawy
 *
 */
public class ATMRequestUpdateDE5 extends AtmMessageLifecycle {

    @Override
    public String getFieldValue(String isoRequestFieldValue, AtmTransactionContext atmTransactionContext,
            IsoRequest isoRequest, String intrfaceMessageId) {

        DataAccess dataAccess = new DataAccess(this);
        Double txnAmt = 0D, fcyAmt = 0D, returnedAmt = 0D, availBal = 0D;
        String stlmntCcyIsoCode = "";
        try {
            stlmntCcyIsoCode = dataAccess.selectRecords(atmTransactionContext.getCompanyMnemonic().toString(),
                    "CURRENCY", "", "WITH NUMERIC.CCY.CODE EQ '" + isoRequest.getSettlementCurrencyCode() + "'").get(0);
        } catch (T24CoreException e) {
            stlmntCcyIsoCode = "EGP";
        }
        CurrencyRecord currency = new CurrencyRecord(dataAccess.getRecord("CURRENCY", stlmntCcyIsoCode));
        Double divVal = Math.pow(10, new Double(currency.getNoOfDecimals().getValue()));

        if (isoRequestFieldValue.contains("%")) {
            fcyAmt = Double.parseDouble(isoRequestFieldValue.split("%")[0]);
            txnAmt = Double.parseDouble(isoRequestFieldValue.split("%")[1]);
            if (fcyAmt != 0) {
                returnedAmt = fcyAmt / divVal;
            } else {
                returnedAmt = txnAmt / divVal;
            }
        } else {
            txnAmt = new Double(isoRequestFieldValue);
            returnedAmt = txnAmt / divVal;
        }

        if (!isoRequest.getAdditionalIsoData().isEmpty()) {
            String[] charges = isoRequest.getAdditionalIsoData().split("\\s+");
            for (String chg : charges) {
                txnAmt += Double.parseDouble(chg.substring(6, chg.length()));
            }
            returnedAmt = txnAmt / divVal;
        }

        // check if the iso message allow partial approval
        if (!isoRequest.getAdditionalPrivateData().isEmpty() && isoRequest.getAdditionalPrivateData().equals("1")) {

            // get company record to use Mnemonic
            CompanyRecord company = new CompanyRecord(dataAccess.getRecord("COMPANY", "EG0011001"));

            // get T24 today's date
            String sT24Date = new DatesRecord(dataAccess.getRecord("DATES", "EG0011001")).getToday().getValue();
            Date dT24Date = null;
            try {
                dT24Date = new SimpleDateFormat("YYYYMMDD").parse(sT24Date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Read account details from account table
            AccountRecord account = new AccountRecord(dataAccess.getRecord(company.getMnemonic().getValue(), "ACCOUNT",
                    "", isoRequest.getAccountNumber().trim()));
            // get account working balance
            availBal += Double.parseDouble(account.getWorkingBalance().getValue());

            // start calculating limits
            String limitRef = account.getLimitRef().getValue();
            if (!limitRef.isEmpty()) {
                LimitRecord limit = new LimitRecord(
                        dataAccess.getRecord(company.getMnemonic().getValue(), "LIMIT", "", limitRef.trim()));
                if (limit != null) {
                    availBal += Double.parseDouble(limit.getCurrUnutilAmt().getValue());
                }
            }
            // start calculate locked amounts
            List<String> lockedEvtIds = new ArrayList<String>();
            lockedEvtIds = dataAccess.selectRecords(company.getMnemonic().getValue(), "AC.LOCKED.EVENTS", "",
                    "WITH ACCOUNT.NUMBER EQ '" + isoRequest.getAccountNumber() + "'");
            Double totalLockedAmt = 0D;
            for (String lckEvtId : lockedEvtIds) {
                AcLockedEventsRecord acLockedEvt = new AcLockedEventsRecord(
                        dataAccess.getRecord(company.getMnemonic().getValue(), "AC.LOCKED.EVENTS", "", lckEvtId));

                // to check the locked amount record period is active
                Date fromDt = null, toDt = null;
                String toDate = "";
                try {
                    fromDt = new SimpleDateFormat("YYYYMMDD").parse(acLockedEvt.getFromDate().getValue());
                    toDate = acLockedEvt.getToDate().getValue();
                    if (!toDate.equals("")) {
                        toDt = new SimpleDateFormat("YYYYMMDD").parse(toDate);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if ((fromDt.before(dT24Date) && (toDt.after(dT24Date))
                        || (fromDt.before(dT24Date) && toDate == null))) {
                    totalLockedAmt += Double.parseDouble(acLockedEvt.getLockedAmount().getValue());
                }
            }
            availBal -= totalLockedAmt;

            if (availBal > 0 && availBal < returnedAmt) {
                // We have to flag any field that T24 changed the amount
                return availBal.toString();
            }
        }

        return returnedAmt.toString();
    }

}