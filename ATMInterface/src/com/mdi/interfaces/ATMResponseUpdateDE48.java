/**
 * @author Kareem Mortada
 * @email KMortada@itssglobal.com
 * @create date 2022-04-25 14:58:47
 * @modify date 2022-04-25 14:58:47
 * @desc [description]
 */
package com.mdi.interfaces;

import com.temenos.api.exceptions.T24CoreException;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.acinwardentry.AcInwardEntryRecord;
import com.temenos.t24.api.records.atmtransaction.AtmTransactionRecord;
import com.temenos.t24.api.system.DataAccess;

public class ATMResponseUpdateDE48 extends AtmMessageLifecycle {

    @Override
    public String getFieldValue(String isoRequestFieldValue, AtmTransactionContext atmTransactionContext,
            IsoRequest isoRequest, String intrfaceMessageId) {
        DataAccess dataAccess = new DataAccess(this);

        String uniqueID = isoRequest.getMessageTypeIndicator() + isoRequest.getProcessingCode()
                + isoRequest.getLocalTransactionDate() + isoRequest.getRetrievalReferenceNumber()
                + isoRequest.getAccountNumber();
        try {
            AtmTransactionRecord atmTransaction = new AtmTransactionRecord(
                    dataAccess.getRecord("ATM.TRANSACTION", uniqueID.trim()));

            AcInwardEntryRecord acInwardEntry = new AcInwardEntryRecord(
                    dataAccess.getRecord(atmTransactionContext.getCompanyMnemonic(), "AC.INWARD.ENTRY", "",
                            atmTransaction.getTransRef(0).getValue()));

            if (!acInwardEntry.getAmountLcy().getValue().equals(isoRequest.getSettlementAmount()))
                return "2";
            else
                return "1";
        } catch (T24CoreException e) {
            return "0";
        }
    }
}