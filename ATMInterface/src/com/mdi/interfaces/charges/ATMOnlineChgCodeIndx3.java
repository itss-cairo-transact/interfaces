package com.mdi.interfaces.charges;

import com.temenos.api.LocalRefList;
import com.temenos.api.TNumber;
import com.temenos.t24.api.complex.atmfrm.messagehook.AtmTransactionContext;
import com.temenos.t24.api.complex.atmfrm.messagehook.Charge;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequest;
import com.temenos.t24.api.complex.atmfrm.messagehook.IsoRequestHeader;
import com.temenos.t24.api.hook.atm.AtmMessageLifecycle;
import com.temenos.t24.api.records.account.AccountRecord;
import com.temenos.t24.api.records.atmchgtable.AtmChgTableRecord;
import com.temenos.t24.api.records.atmparameter.AtmParameterRecord;
import com.temenos.t24.api.records.atmtransaction.AtmTransactionRecord;
import com.temenos.t24.api.records.currency.CurrencyRecord;
import com.temenos.t24.api.system.DataAccess;

/**
 * TODO: Document me!
 *
 * @author alargawy
 *
 */
public class ATMOnlineChgCodeIndx3 extends AtmMessageLifecycle {

    @Override
    public Charge getCharge(AtmTransactionContext transactionContext, IsoRequest isoRequest,
            IsoRequestHeader isoRequestHeader, AtmTransactionRecord transactionRecord) {

        DataAccess dataAccess = new DataAccess(this);
        Charge chg = new Charge();

        // get third charge block only in this method
        String chgBlock = isoRequest.getAdditionalIsoData(); //102
        if (!chgBlock.isEmpty()) {
            try {
                chgBlock = chgBlock.split("\\s+")[2];
                AtmParameterRecord atmParam = new AtmParameterRecord(dataAccess.getRecord("ATM.PARAMETER", "SYSTEM"));

                String chgCode = chgBlock.substring(0, 2);
                String chgSign = chgBlock.substring(5, 6);
                String chgCcy = "";
                switch (atmParam.getChgCcyPos().getValue()) {
                case "49":
                    chgCcy = isoRequest.getTransactionCurrency();
                    break;
                case "50":
                    chgCcy = isoRequest.getSettlementCurrencyCode();
                    break;
                case "51":
                    chgCcy = isoRequest.getCardholderBillingCurrencyCode();
                    break;
                default:
                    chgCcy = chgBlock.substring(2, 5);
                }

                String chgCcyIsoCode = dataAccess.selectRecords(transactionContext.getCompanyMnemonic().toString(),
                        "CURRENCY", "", "WITH NUMERIC.CCY.CODE EQ '" + chgCcy + "'").get(0);
                CurrencyRecord chgCcyRec = new CurrencyRecord(dataAccess.getRecord("CURRENCY", chgCcyIsoCode));
                TNumber tNumChgAmt = new TNumber(Double.parseDouble(chgBlock.substring(6, 14))
                        / Math.pow(10, Double.parseDouble(chgCcyRec.getNoOfDecimals().getValue())));

                AccountRecord acctRec = new AccountRecord(
                        dataAccess.getRecord("ACCOUNT", isoRequest.getAccountNumber().trim()));
                CurrencyRecord acctCcyRec = new CurrencyRecord(
                        dataAccess.getRecord("CURRENCY", acctRec.getCurrency().getValue()));
                if (!chgCcyIsoCode.equals(acctRec.getCurrency().getValue())) {
                    tNumChgAmt.set(Double.parseDouble(tNumChgAmt.get())
                            * Double.parseDouble(acctCcyRec.getCurrencyMarket().get(1).getMidRevalRate().getValue()));
                }

                AtmChgTableRecord atmChgTblRec = new AtmChgTableRecord(dataAccess.getRecord("ATM.CHG.TABLE", "SYSTEM"));

                String creditAcct = "";
                String txnCode = "";
                LocalRefList lRefL = atmChgTblRec.getLocalRefGroups("L.CHG.CODE");
                for (int i = 0; i < lRefL.size(); i++) {
                    if (lRefL.get(i).getLocalRefField("L.CHG.CODE").getValue().equals(chgCode)) {
                        if (lRefL.get(i).getLocalRefField("L.CHG.PL.ACCT") != null) {
                            creditAcct = lRefL.get(i).getLocalRefField("L.CHG.PL.ACCT").getValue();
                        } else {
                            creditAcct = lRefL.get(i).getLocalRefField("L.CHG.SUS.ACCT").getValue();
                            creditAcct = chgCcyIsoCode.trim() + creditAcct;
                        }
                        txnCode = lRefL.get(i).getLocalRefField("L.CHG.TXN.CODE").getValue();
                    }
                }
                chg.setCreditAccount(creditAcct);
                chg.setAmount(tNumChgAmt);
                chg.setTransactionCode(new TNumber(txnCode));

                // if (chg.getAmount().get().equals("0")) {
                // return super.getCharge(transactionContext, isoRequest,
                // isoRequestHeader, transactionRecord);
                // } else {
                return chg;
                // }
            } catch (Exception e) {
                return chg;
            }
        } else {
            return chg;
        }
    }
}
