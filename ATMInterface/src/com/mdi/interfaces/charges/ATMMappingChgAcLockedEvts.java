package com.mdi.interfaces.charges;

import com.temenos.api.TStructure;
import com.temenos.t24.api.complex.eb.templatehook.TransactionContext;
import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.t24.api.records.aclockedevents.AcLockedEventsRecord;
import com.temenos.t24.api.records.currency.CurrencyRecord;
import com.temenos.t24.api.system.DataAccess;

/**
 * TODO: Document me!
 *
 * @author alargawy
 *
 */
public class ATMMappingChgAcLockedEvts extends RecordLifecycle {

    @Override
    public void defaultFieldValues(String application, String currentRecordId, TStructure currentRecord,
            TStructure unauthorisedRecord, TStructure liveRecord, TransactionContext transactionContext) {

        DataAccess dataAccess = new DataAccess(this);
        AcLockedEventsRecord acLkdEvts = new AcLockedEventsRecord(currentRecord);

        Double txnAmt = Double.parseDouble(acLkdEvts.getLockedAmount().getValue());
        if (!acLkdEvts.getAddDetailReqSource().getValue().equals("")) {
            String[] chargesDE46 = acLkdEvts.getAddDetailReqSource().getValue().split("%");
            Double[] chgAmts = { 0D, 0D, 0D };
            Double totalChargeAmts = 0D;
            for (int i = 0; i < chargesDE46.length; i++) {
                String chgBlock = chargesDE46[i];
                String chgCcy = chgBlock.substring(2, 5);
                String chgCcyIsoCode = dataAccess
                        .selectRecords("ODB", "CURRENCY", "", "WITH NUMERIC.CCY.CODE EQ '" + chgCcy + "'").get(0);
                CurrencyRecord chgCcyRec = new CurrencyRecord(dataAccess.getRecord("CURRENCY", chgCcyIsoCode));
                double chgAmt = (Double.parseDouble(chgBlock.substring(6, 14))
                        / Math.pow(10, Double.parseDouble(chgCcyRec.getNoOfDecimals().getValue())));
                totalChargeAmts += chgAmt;
                chgAmts[i] = chgAmt;
            }
            txnAmt -= totalChargeAmts;
            acLkdEvts.getLocalRefField("L.ORIGINAL.AMT").set(txnAmt.toString());
            acLkdEvts.getLocalRefField("L.TOTAL.CHRG.AMT").set(totalChargeAmts.toString());
            acLkdEvts.getLocalRefField("L.CHARGE.AMT1").set(chgAmts[0].toString());
            acLkdEvts.getLocalRefField("L.CHARGE.AMT2").set(chgAmts[1].toString());
            acLkdEvts.getLocalRefField("L.CHARGE.AMT3").set(chgAmts[2].toString());

            currentRecord.set(acLkdEvts.toStructure());
        }
    }
}
