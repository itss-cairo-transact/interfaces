* @ValidationCode : Mjo4MTA3OTIzODU6Q3AxMjUyOjE2NTM5MDQ4OTIwNDY6YWxhcmdhd3k6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 May 2022 12:01:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : alargawy
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
$PACKAGE EB.MDIATMADT
SUBROUTINE EB.MDI.ADT.FILE.ADJST.AMT
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING FT.Clearing
    $USING EB.DataAccess
*    $USING ATMFRM.Mapping
    $USING AC.AccountOpening
    $USING LI.LimitTransaction
    $USING AC.CashFlow
   

    GOSUB INITIALISE;
    GOSUB PROCESS;
    
RETURN

INITIALISE:
    
    COMI = EB.SystemTables.getComi()
    OrigRawMsg  = FT.Clearing.getoriginalRawMessage()
    BodyMsg = FIELD(FIELD(OrigRawMsg, '|', 2), '~', 2)
    
    DebitAcctNo = BodyMsg<2>
    DebitTxnAmt = BodyMsg<7>

    PlCategory = BodyMsg<17>
    ContraCateg = BodyMsg<18>
    CreditTxnAmt = BodyMsg<21>
    
    WorkingBal = 0
    AvailBal = 0
    LimitRef = 0
    CusId = 0
    LimitAmt = ''
    RAcct = ''
    Ccy = ''
    Available = '1'
    LiabNo = ''
    LockedDetails = ''
    ResponseDetails = ''
    
RETURN

PROCESS:
    
    IF DebitAcctNo THEN
        FNAcct = 'FODB.ACCOUNT'
        FAcct = ''
        EB.DataAccess.Opf(FNAcct, FAcct)
        RAcct = AC.AccountOpening.Account.Read(DebitAcctNo, ERR)
        CRT 'RAcct ----------:': RAcct
*ATMFRM.Mapping.AtCalcAvailBalance(DebitAcctNo,RAcct,WorkingBal,AvailBal)
        
        CusId = RAcct<AC.AccountOpening.Account.Customer>
        AvailBal = AvailBal + RAcct<AC.AccountOpening.Account.WorkingBalance>
        LimitRef = RAcct<AC.AccountOpening.Account.LimitRef>
        Ccy = RAcct<AC.AccountOpening.Account.Currency>
        LI.LimitTransaction.GetAccountLimitAmts(LiabNo,CusId,LimitRef,Ccy,LimitAmt,Available)
        AvailBal = AvailBal + LimitAmt

        AC.CashFlow.GetLockedDetails(DebitAcctNo, LockedDetails,ResponseDetails)
        LockedAmt = LockedDetails<2>
        LockedDates = LockedDetails<1>
        IF LockedDates THEN
            U.CTR = DCOUNT(LockedDates,@VM)
            U.I = 1
            LOOP
            WHILE U.I LE U.CTR
                IF EB.SystemTables.getToday() GE LockedDates<1,U.I> THEN
                    ULockAmt = LockedAmt<1,U.I>
                END
                U.I+=1
            REPEAT
        END
       
        AvailBal = AvailBal - ULockAmt

        CRT 'AVAILABLE BALANCE ----------:': AvailBal
        IF AvailBal GT 0 AND AvailBal LT DebitTxnAmt THEN
            CRT 'TXN AMT UPDATED ----------:'
            COMI = AvailBal
        END ELSE
            CRT 'TXN AMT AS IS ----------:'
            COMI = DebitTxnAmt
        END
    END
    
*IF PlCategory OR ContraCateg THEN
*    IF DebitTxnAmt NE CreditTxnAmt THEN
*        COMI = DebitTxnAmt
*    END
*END
       
    EB.SystemTables.setComi(COMI)
    
RETURN

END
