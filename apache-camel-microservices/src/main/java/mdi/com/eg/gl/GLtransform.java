package mdi.com.eg.gl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;

public class GLtransform implements Processor {

	ProducerTemplate producer;
	String regex = "^0+(?!$)";
	String ofsMessage;

	public void setProducer(ProducerTemplate producer) {
		this.producer = producer;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String actualMsg = exchange.getIn().getBody(String.class);
		final String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		System.setProperty("fileName", camelFileName);
		System.out.println(actualMsg);
		String[] lines;
		lines = actualMsg.split("\\R");
		int linesSize = lines.length;
		lines = Arrays.copyOfRange(lines,1,linesSize-1);
		linesSize = lines.length;
		String currentLine;
		
		try {
		if (linesSize > 0) {
			GLmappingHeader glHeader = new GLmappingHeader();
			GLmappingRecord glRecord = new GLmappingRecord();
			GLmappingTrailer glTrailer = new GLmappingTrailer();
			List<String> test1 = new ArrayList<>();

			for (int i = 0; i < linesSize; i++) {
				currentLine = lines[i];
				int currentLineSize = currentLine.length();

				if (i == 0) {
					if (currentLineSize > 0) {
						glHeader.setRecordType(currentLine.charAt(0));
						glHeader.setInstitutionCode(currentLine.substring(1, 5).replaceAll("\\s", ""));
						glHeader.setSystemDate(currentLine.substring(5, 13).replaceAll("\\s", ""));
						glHeader.setFiller(currentLine.substring(13, currentLineSize).replaceAll("\\s", ""));

					}
				} else if (i == linesSize - 1) {
					glTrailer.setRecordType(currentLine.charAt(0));
					glTrailer.setNumberOfRecords(currentLine.substring(1, 11).replaceAll("\\s", ""));
					glTrailer.setPostingAmount(currentLine.substring(11, 41).replaceAll("\\s", ""));
					glTrailer.setFiller(currentLine.substring(41, currentLineSize).replaceAll("\\s", ""));

				} else {
					
						glRecord.setRecordType(currentLine.charAt(0));
						glRecord.setInstitutionCode(currentLine.substring(1, 5).replaceAll("\\s", ""));
						glRecord.setUniqueId(currentLine.substring(5, 25).replaceAll("\\s", ""));
						glRecord.setTransactionId(currentLine.substring(25, 45).replaceAll("\\s", ""));
						glRecord.setItemSource(currentLine.substring(45, 53).replaceAll("\\s", ""));
						glRecord.setItemReference(currentLine.substring(53, 117).replaceAll("\\s", ""));
						glRecord.setSystemDate(currentLine.substring(117, 125).replaceAll("\\s", ""));
						glRecord.setCortexAccountNumber(currentLine.substring(125, 153).replaceAll("\\s", ""));
						glRecord.setCardIdentifier(currentLine.substring(153, 185).replaceAll("\\s", ""));
						glRecord.setAccountToDebit(currentLine.substring(185, 213).replaceAll("\\s", ""));
						glRecord.setAccountToCredit(currentLine.substring(213, 241).replaceAll("\\s", ""));
						glRecord.setEventCode(currentLine.substring(241, 273).replaceAll("\\s", ""));
						String currenctDes = currentLine.substring(273, 401).replaceAll("\\s", "");
						int currentDesLength = currenctDes.length();
						if (currentDesLength>=35) {
						glRecord.setDescription(currenctDes.substring(0,34));
						}else {
							glRecord.setDescription(currenctDes.substring(0,currentDesLength));
						}	
						glRecord.setDescriptionExtended(currentLine.substring(401, 656).replaceAll("\\s", ""));
						glRecord.setCreditDebitFlag(currentLine.charAt(656));
						String postingAmount = currentLine.substring(657, 677).replaceAll("\\s", "");
						postingAmount = postingAmount.replaceAll(regex, "");
						glRecord.setPostingAmount(postingAmount);
						glRecord.setPostingCurrency(currentLine.substring(677, 680).replaceAll("\\s", ""));
						glRecord.setTransactionAmount(currentLine.substring(680, 700).replaceAll("\\s", ""));
						glRecord.setTransactionCurrency(currentLine.substring(700, 703).replaceAll("\\s", ""));
						glRecord.setAdditionalAmount1(currentLine.substring(703, 723).replaceAll("\\s", ""));
						glRecord.setAdditionalAmount1Currency(currentLine.substring(723, 726).replaceAll("\\s", ""));
						glRecord.setAdditionalAmount2(currentLine.substring(726, 746).replaceAll("\\s", ""));
						glRecord.setAdditionalAmount2Currency(currentLine.substring(746, 749).replaceAll("\\s", ""));
						glRecord.setRrn(currentLine.substring(749, 761).replaceAll("\\s", ""));
						glRecord.setAcquirerCode(currentLine.substring(761, 769).replaceAll("\\s", ""));
						glRecord.setIssuerCode(currentLine.substring(769, 777).replaceAll("\\s", ""));
						glRecord.setProductCode(currentLine.substring(777, currentLineSize).replaceAll("\\s", ""));
						////////////////////// START OFS MAPPING //////////////////////
						String cortexAcEntryParam = properties.getProperty("CORTEX.AC_ENTRY_PARAM");
						String dataFeild = "0000";
						String ofsMessage = "CSMBULK=BOOK,,//////," + cortexAcEntryParam
								+ "," + dataFeild // DATA
								+ "," + glRecord.getUniqueId() // THEIR.REF
								+ "," + glRecord.getTransactionId() // TRANS.REFERENCE
								+ "," + glRecord.getSystemDate() // VALUE.DATE
								+ "," + glRecord.getAccountToDebit() // ACCOUNT
								+ "," // PL.CATEGORY
								+ "," + glRecord.getDescription() // NARRATIVE
								+ "," + "D" // SIGN
								+ "," + glRecord.getPostingAmount() // AMOUNT
								+ "," + glRecord.getPostingCurrency() // CURRENCY
								+ "," + glRecord.getProductCode() // DEPARTMENT
								+ "," + System.getProperty("fileName")!=null?System.getProperty("fileName"):"CGLP" // LOCAL.REF-L.FILE.NAME
								+ "," + glRecord.getAccountToCredit() // LOCAL.REF-L.ACCT
								+ "," + glRecord.getRrn() // LOCAL.REF-L.RRN
								+ "," + glRecord.getAcquirerCode() // LOCAL.REF-L.ACQ.CODE
								+ "," + glRecord.getIssuerCode() // LOCAL.REF-L.ISSUER.CODE
								+ "_" // SEPRATOR
								
								+ "," + dataFeild // DATA
								+ "," + glRecord.getUniqueId() // THEIR.REF
								+ "," + glRecord.getTransactionId() // TRANS.REFERENCE
								+ "," + glRecord.getSystemDate() // VALUE.DATE
								+ "," // ACCOUNT
								+ "," + glRecord.getAccountToCredit() // PL.CATEGORY
								+ "," + glRecord.getDescription() // NARRATIVE
								+ "," + "C" // SIGN
								+ "," + glRecord.getPostingAmount() // AMOUNT
								+ "," + glRecord.getPostingCurrency() // CURRENCY
								+ "," + glRecord.getProductCode() // DEPARTMENT
								+ "," + System.getProperty("fileName")!=null?System.getProperty("fileName"):"CGLP" // LOCAL.REF-L.FILE.NAME
								+ "," + glRecord.getAccountToDebit() // LOCAL.REF-L.ACCT
								+ "," + glRecord.getRrn() // LOCAL.REF-L.RRN
								+ "," + glRecord.getAcquirerCode() // LOCAL.REF-L.ACQ.CODE
								+ "," + glRecord.getIssuerCode(); // LOCAL.REF-L.ISSUER.CODE
						System.out.println(ofsMessage);
						
						
						////////////////////// END OFS MAPPING //////////////////////
					

					test1.add(ofsMessage);

				}
			}
			System.out.println(test1.size());
			exchange.getIn().setBody(test1);
		}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

}
