package mdi.com.eg.gl;

public class GLmappingTrailer {

	char recordType;
	String NumberOfRecords;
	String postingAmount;
	String filler;
	
	public char getRecordType() {
		return recordType;
	}
	public void setRecordType(char recordType) {
		this.recordType = recordType;
	}
	public String getNumberOfRecords() {
		return NumberOfRecords;
	}
	public void setNumberOfRecords(String numberOfRecords) {
		NumberOfRecords = numberOfRecords;
	}
	public String getPostingAmount() {
		return postingAmount;
	}
	public void setPostingAmount(String postingAmount) {
		this.postingAmount = postingAmount;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
}
