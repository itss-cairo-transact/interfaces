package mdi.com.eg.gl;

public class GLmappingRecord {
	char recordType;
	String institutionCode;
	String uniqueId;
	String transactionId;
	String itemSource;
	String itemReference;
	String systemDate;
	String cortexAccountNumber;
	String cardIdentifier;
	String accountToDebit;
	String accountToCredit;
	String eventCode;
	String description;
	String descriptionExtended;
	char creditDebitFlag;
	String postingAmount;
	String postingCurrency;
	String transactionAmount;
	String transactionCurrency;
	String additionalAmount1;
	String additionalAmount1Currency;
	String additionalAmount2;
	String additionalAmount2Currency;
	String rrn;
	String acquirerCode;
	String issuerCode;
	String productCode;
	public char getRecordType() {
		return recordType;
	}
	public void setRecordType(char recordType) {
		this.recordType = recordType;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getItemSource() {
		return itemSource;
	}
	public void setItemSource(String itemSource) {
		this.itemSource = itemSource;
	}
	public String getItemReference() {
		return itemReference;
	}
	public void setItemReference(String itemReference) {
		this.itemReference = itemReference;
	}
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	public String getCortexAccountNumber() {
		return cortexAccountNumber;
	}
	public void setCortexAccountNumber(String cortexAccountNumber) {
		this.cortexAccountNumber = cortexAccountNumber;
	}
	public String getCardIdentifier() {
		return cardIdentifier;
	}
	public void setCardIdentifier(String cardIdentifier) {
		this.cardIdentifier = cardIdentifier;
	}
	public String getAccountToDebit() {
		return accountToDebit;
	}
	public void setAccountToDebit(String accountToDebit) {
		this.accountToDebit = accountToDebit;
	}
	public String getAccountToCredit() {
		return accountToCredit;
	}
	public void setAccountToCredit(String accountToCredit) {
		this.accountToCredit = accountToCredit;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptionExtended() {
		return descriptionExtended;
	}
	public void setDescriptionExtended(String descriptionExtended) {
		this.descriptionExtended = descriptionExtended;
	}
	public char getCreditDebitFlag() {
		return creditDebitFlag;
	}
	public void setCreditDebitFlag(char creditDebitFlag) {
		this.creditDebitFlag = creditDebitFlag;
	}
	public String getPostingAmount() {
		return postingAmount;
	}
	public void setPostingAmount(String postingAmount) {
		this.postingAmount = postingAmount;
	}
	public String getPostingCurrency() {
		return postingCurrency;
	}
	public void setPostingCurrency(String postingCurrency) {
		this.postingCurrency = postingCurrency;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getTransactionCurrency() {
		return transactionCurrency;
	}
	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}
	public String getAdditionalAmount1() {
		return additionalAmount1;
	}
	public void setAdditionalAmount1(String additionalAmount1) {
		this.additionalAmount1 = additionalAmount1;
	}
	public String getAdditionalAmount1Currency() {
		return additionalAmount1Currency;
	}
	public void setAdditionalAmount1Currency(String additionalAmount1Currency) {
		this.additionalAmount1Currency = additionalAmount1Currency;
	}
	public String getAdditionalAmount2() {
		return additionalAmount2;
	}
	public void setAdditionalAmount2(String additionalAmount2) {
		this.additionalAmount2 = additionalAmount2;
	}
	public String getAdditionalAmount2Currency() {
		return additionalAmount2Currency;
	}
	public void setAdditionalAmount2Currency(String additionalAmount2Currency) {
		this.additionalAmount2Currency = additionalAmount2Currency;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getAcquirerCode() {
		return acquirerCode;
	}
	public void setAcquirerCode(String acquirerCode) {
		this.acquirerCode = acquirerCode;
	}
	public String getIssuerCode() {
		return issuerCode;
	}
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
}
