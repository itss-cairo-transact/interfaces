package mdi.com.eg.gl;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import mdi.com.eg.camelmicroservices.replyProcessor;



@Component
public class GLroutes extends RouteBuilder {
	Hashtable<Object, String> env = new Hashtable<Object, String>();

	@Override
	public void configure() throws Exception {

		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String from = properties.getProperty("CORTEX.sourceFolder");
		String mqVariable = properties.getProperty("CORTEX.queue");
		String queueProvider = properties.getProperty("queueProvider");
		String replyQueue = properties.getProperty("CORTEX.replyQueue");
		String replyDir = properties.getProperty("CORTEX.replyFolder");

		
		from(from+"?move=backup/${file:onlyname}")
		.bean(GLtransform.class)
		.split(body())
		.setExchangePattern(ExchangePattern.InOut)
		.to(queueProvider+":"+mqVariable+"?replyTo="+replyQueue)
		.bean(replyProcessor.class)
		.transform(body().append("\n"))
		.to(replyDir+"?fileExist=Append&charset=utf-8");
		
		
	}

}
