package mdi.com.eg.atd;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDtransformBalance implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String actualMsg = exchange.getIn().getBody(String.class);
		System.setProperty("originalMsg", actualMsg);
		final String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		System.setProperty("fileName", camelFileName);
		String[] lines;
		lines = actualMsg.split("\\R");
		int linesSize = lines.length;
		String currentLine;
		try {
			if (linesSize > 0) {
				currentLine = lines[0];
				if (currentLine.charAt(0) != 'H' && currentLine.charAt(0) != 'T') {
					String currentAccount = currentLine.substring(221, 249).replaceAll("\\s", "");
					String ofsMessage = "MDI.CALCULATE.AVAILABLE.BALANCE,,INPUTT/123456,," + currentAccount;
					
					exchange.getIn().setBody(ofsMessage);
					System.out.println(ofsMessage);
				} else if (currentLine.charAt(0) == 'H')
				{
					System.setProperty("headerMsg",actualMsg);
					exchange.getIn().setBody("");
				}
				else if (currentLine.charAt(0) == 'T')
				{
					System.setProperty("traillerMsg",actualMsg);
					exchange.getIn().setBody("");
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
