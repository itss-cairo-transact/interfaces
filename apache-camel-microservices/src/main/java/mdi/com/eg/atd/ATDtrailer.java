package mdi.com.eg.atd;

public class ATDtrailer {
	String recordType;
	String numberOfRecords;
	String amountToCollect;
	String filler;
	
	public ATDtrailer()
	{}
	
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getNumberOfRecords() {
		return numberOfRecords;
	}
	public void setNumberOfRecords(String numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}
	public String getAmountToCollect() {
		return amountToCollect;
	}
	public void setAmountToCollect(String amountToCollect) {
		this.amountToCollect = amountToCollect;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
}
