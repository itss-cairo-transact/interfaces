package mdi.com.eg.atd;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDreplyProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String responseMsg = exchange.getIn().getBody(String.class);

		System.out.println("***************************************************************************");
		System.out.println(responseMsg);

		System.setProperty("ofsResponse", responseMsg);

		String[] resMsg = responseMsg.split("/");
		if (responseMsg.equals("0,0,0///")) {

			String filename = System.getProperty("fileName");
			exchange.getIn().setHeader(Exchange.FILE_NAME, filename);
			String originalMsg = System.getProperty("originalMsg");
			String currentLine;
			String autodebitReference = resMsg[0];
			if (originalMsg.charAt(0) == 'H') {
				exchange.getIn().setBody(originalMsg);
			} else if (originalMsg.charAt(0) == 'T') {
				exchange.getIn().setBody(originalMsg);
			} else {
				currentLine = "R";
				currentLine = currentLine + System.getProperty("InstitutionCode") + System.getProperty("UniqueId");
				int autodebitReferenceLen = autodebitReference.length();
				for (int i = 0; i < 64 - autodebitReference.length(); i++)
					autodebitReference = autodebitReference + " ";
				currentLine = currentLine + autodebitReference + System.getProperty("SystemDate")
						+ System.getProperty("ChargeProfile") + System.getProperty("ChargeSchema")
						+ System.getProperty("CustomerCode") + System.getProperty("AccountNumber")
						+ System.getProperty("AccountCurrency") + System.getProperty("Vpan")
						+ System.getProperty("MaskedPan") + System.getProperty("CreditLimit")
						+ System.getProperty("OutsandingBalance") + System.getProperty("DueAmount");
				String currentAmount = System.getProperty("AmountToCollect");
				for (int i = 0; i < 28 - currentAmount.length(); i++)
					currentAmount = currentAmount + " ";
				currentLine = currentLine + System.getProperty("originalAmount")
						+ System.getProperty("DirectDebitAccount") + currentAmount;
				System.out.println(currentLine);
				String collectedAmount = System.getProperty("TotalAmount");
				float collectedAmountFloat = Float.valueOf(collectedAmount);
				float currentAmountFloat = Float.valueOf(currentAmount);
				collectedAmountFloat = collectedAmountFloat + currentAmountFloat;
				collectedAmount = String.valueOf(collectedAmountFloat);
				System.setProperty("TotalAmount", collectedAmount);
				System.out.println("*******************************************************");
				System.out.println(System.getProperty("TotalAmount"));

				exchange.getIn().setBody(currentLine);

			}
		}
	}

}
