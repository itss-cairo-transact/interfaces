package mdi.com.eg.atd;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDtransform implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		
		String actualMsg = exchange.getIn().getBody(String.class);
		System.setProperty("originalMsg", actualMsg);
		String ofsToQueue = "MDI.CALCULATE.AVAILABLE.BALANCE,,INPUTT/123456,," + actualMsg;
		exchange.getIn().setBody(ofsToQueue);
	}

}
