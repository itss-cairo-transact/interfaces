package mdi.com.eg.atd;

import java.io.InputStream;
import java.util.Properties;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ATDroute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String from = properties.getProperty("ATD.sourceFolder");
		String to = properties.getProperty("ATD.queue");
		String queueProvider = properties.getProperty("queueProvider");
		String replyDir = properties.getProperty("ATD.replyFolder");
		String replyQueue = properties.getProperty("ATD.replyQueue");
		String tempDir = properties.getProperty("ATD.tempFolder");

		from(from + "?move=backup/${file:onlyname}")
		.routeId("route1")
		.bean(ATDinitialProcessor.class)
		.split(body())
		.bean(ATDtransform.class)
				// .to(queueProvider+":"+to+"?replyTo="+replyQueue)
				//.bean(ATDreplyProcessor.class)
				.transform(body().append("\n"))
				.log("File: still processing")
				.to(tempDir + "?fileExist=Append&charset=utf-8")
				.end()
				.log("File: ${headers.CamelFileName} successfully processed")
				.pollEnrich(replyDir)
				.bean(ATDreplyProcessor2.class)
				.to(replyDir + "?fileExist=Append&charset=utf-8");

//		from(from+"?move=backup/${file:onlyname}")
//		//.bean(ATDinitialProcessor.class)
//		//.split(body())
//		//.setExchangePattern(ExchangePattern.InOut)
//		//.bean(ATDtransformBalance.class)
//		//.to(queueProvider+":"+to+"?replyTo="+replyQueue)
//		.bean(ATDtransform2.class)
//		//.to(queueProvider+":"+to+"?replyTo="+replyQueue)
//		//.log("InOut MEP received ${body}")
//		.to(queueProvider+":"+to+"?replyTo="+replyQueue)
//		.bean(ATDreplyProcessor.class)
//		.transform(body().append("\n"))
//		.to(replyDir+"?fileExist=Append&charset=utf-8")
//		.bean(ATDtransformLog.class)
//		.transform(body().append("\n"))
//		.to(logDir+"?fileExist=Append&charset=utf-8");

	}

}
