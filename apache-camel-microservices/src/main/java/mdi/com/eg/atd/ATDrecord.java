package mdi.com.eg.atd;

public class ATDrecord {

	String recordType;
	String institutionCode;
	String uniqueId;
	String systemDate;
	String chargeProfile;
	String chargeSchema;
	String customerCode;
	String accountNumber;
	String accountCurrency;
	String vpan;
	String maskedPan;
	String creditLimit;
	String outsandingBalance;
	String dueAmount;
	String amountToCollect;
	public String getAmountToCollect() {
		return amountToCollect;
	}
	public void setAmountToCollect(String amountToCollect) {
		this.amountToCollect = amountToCollect;
	}
	String directDebitAccount;
	
	public ATDrecord()
	{
		
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	public String getChargeProfile() {
		return chargeProfile;
	}
	public void setChargeProfile(String chargeProfile) {
		this.chargeProfile = chargeProfile;
	}
	public String getChargeSchema() {
		return chargeSchema;
	}
	public void setChargeSchema(String chargeSchema) {
		this.chargeSchema = chargeSchema;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	public String getVpan() {
		return vpan;
	}
	public void setVpan(String vpan) {
		this.vpan = vpan;
	}
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getOutsandingBalance() {
		return outsandingBalance;
	}
	public void setOutsandingBalance(String outsandingBalance) {
		this.outsandingBalance = outsandingBalance;
	}
	public String getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(String dueAmount) {
		this.dueAmount = dueAmount;
	}
	public String getDirectDebitAccount() {
		return directDebitAccount;
	}
	public void setDirectDebitAccount(String directDebitAccount) {
		this.directDebitAccount = directDebitAccount;
	}
	
}
