package mdi.com.eg.atd;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDreplyProcessor2  implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String responseMsg = exchange.getIn().getBody(String.class);
		
		 System.out.println("***************************************************************************");
		 System.out.println(responseMsg);
	}

}
