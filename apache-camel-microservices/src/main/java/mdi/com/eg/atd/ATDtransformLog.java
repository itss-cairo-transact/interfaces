package mdi.com.eg.atd;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDtransformLog implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = System.getProperty("ofsResponse");
		exchange.getIn().setBody(message);

	}

}
