package mdi.com.eg.atd;

import java.io.InputStream;

import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDtransform2 implements Processor {
	float availablebalanceFloat;
	float amountToCollect;
	String sendOfs = "true";
	String ofsMessage="";
	String finalArray = "";
	String tempHeader;
	String tempTrailer;

	@Override
	public void process(Exchange exchange) throws Exception {

		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String actualMsg = exchange.getIn().getBody(String.class);
		System.out.println(actualMsg);
		String atdAcEntryParam = properties.getProperty("ATD.AC_ENTRY_PARAM");
		String[] lines;
		lines = actualMsg.split("\\R");
		int linesSize = lines.length;
		String currentLine;
		try {
			for (int i = 0; i < linesSize; i++) {
				if (linesSize > 0) {
					ATDheader atdHeader = new ATDheader();
					ATDrecord atdRecord = new ATDrecord();
					ATDtrailer atdTrailer = new ATDtrailer();

					currentLine = lines[i];
					int currentLineSize = currentLine.length();

					if (currentLine.charAt(0) == 'H') {
						char headerRecordType = currentLine.charAt(0);
						atdHeader.setRecordType(Character.toString(headerRecordType));
						System.setProperty("headerRecordType", atdHeader.getRecordType());
						atdHeader.setInstitutionCode(currentLine.substring(1, 5).replaceAll("\\s", ""));
						System.setProperty("headerInstitutionCode", atdHeader.getInstitutionCode());
						atdHeader.setSystemDate(currentLine.substring(5, 13).replaceAll("\\s", ""));
						System.setProperty("headerSystemDate", atdHeader.getSystemDate());
						atdHeader.setFiller(currentLine.substring(13, currentLineSize).replaceAll("\\s", ""));
						System.setProperty("headerFiller", atdHeader.getFiller());

					} else if (currentLine.charAt(0) == 'T') {

						char trailerRecordType = currentLine.charAt(0);
						atdTrailer.setRecordType(Character.toString(trailerRecordType));
						System.setProperty("trailerRecordType", atdTrailer.getRecordType());
						atdTrailer.setNumberOfRecords(currentLine.substring(1, 11).replaceAll("\\s", ""));
						System.setProperty("trailerNumberOfRecords", atdTrailer.getNumberOfRecords());
						atdTrailer.setAmountToCollect(currentLine.substring(11, 41).replaceAll("\\s", ""));
						System.setProperty("trailerAmountToCollect", atdTrailer.getAmountToCollect());
						atdTrailer.setFiller(currentLine.substring(41, currentLineSize).replaceAll("\\s", ""));
						System.setProperty("trailerFiller", atdTrailer.getFiller());
					} else {
						char RecordType = currentLine.charAt(0);
						atdRecord.setRecordType(Character.toString(RecordType));
						System.setProperty("RecordType", atdRecord.getRecordType());
						atdRecord.setInstitutionCode(currentLine.substring(1, 5).replaceAll("\\s", ""));
						System.setProperty("InstitutionCode", currentLine.substring(1, 5));
						atdRecord.setUniqueId(currentLine.substring(5, 25).replaceAll("\\s", ""));
						System.setProperty("UniqueId", currentLine.substring(5, 25));
						atdRecord.setSystemDate(currentLine.substring(25, 33).replaceAll("\\s", ""));
						System.setProperty("SystemDate", currentLine.substring(25, 33));
						atdRecord.setChargeProfile(currentLine.substring(33, 43).replaceAll("\\s", ""));
						System.setProperty("ChargeProfile", currentLine.substring(33, 43));
						atdRecord.setChargeSchema(currentLine.substring(43, 47).replaceAll("\\s", ""));
						System.setProperty("ChargeSchema", currentLine.substring(43, 47));
						atdRecord.setCustomerCode(currentLine.substring(47, 59).replaceAll("\\s", ""));
						System.setProperty("CustomerCode", currentLine.substring(47, 59));
						atdRecord.setAccountNumber(currentLine.substring(59, 87).replaceAll("\\s", ""));
						System.setProperty("AccountNumber", currentLine.substring(59, 87));
						atdRecord.setAccountCurrency(currentLine.substring(87, 90).replaceAll("\\s", ""));
						System.setProperty("AccountCurrency", currentLine.substring(87, 90));
						atdRecord.setVpan(currentLine.substring(90, 122).replaceAll("\\s", ""));
						System.setProperty("Vpan", currentLine.substring(90, 122));
						atdRecord.setMaskedPan(currentLine.substring(122, 141).replaceAll("\\s", ""));
						System.setProperty("MaskedPan", currentLine.substring(122, 141));
						atdRecord.setCreditLimit(currentLine.substring(141, 161).replaceAll("\\s", ""));
						System.setProperty("CreditLimit", currentLine.substring(141, 161));
						atdRecord.setOutsandingBalance(currentLine.substring(161, 181).replaceAll("\\s", ""));
						System.setProperty("OutsandingBalance", currentLine.substring(161, 181));
						atdRecord.setDueAmount(currentLine.substring(181, 201).replaceAll("\\s", ""));
						System.setProperty("DueAmount", currentLine.substring(181, 201));
						atdRecord.setAmountToCollect(currentLine.substring(201, 221).replaceAll("\\s", ""));

						System.setProperty("AmountToCollect", currentLine.substring(201, 221));
						atdRecord.setDirectDebitAccount(currentLine.substring(221, 249).replaceAll("\\s", ""));
						System.setProperty("DirectDebitAccount", currentLine.substring(221, 249));
						System.setProperty("processed", sendOfs);
					}
					////////////////////// START OFS MAPPING //////////////////////
					if (currentLine.charAt(0) != 'T' && currentLine.charAt(0) != 'H' && sendOfs != "false") {

						ofsMessage = "CSMBULK=BOOK,,//////," + atdAcEntryParam + "," + atdRecord.uniqueId + ","
								+ atdRecord.systemDate + "," + atdRecord.chargeSchema + "," + atdRecord.accountNumber
								+ "," + atdRecord.accountCurrency + "," + atdRecord.maskedPan + ","
								+ atdRecord.amountToCollect + ",,C_" + atdRecord.uniqueId + "," + atdRecord.systemDate
								+ "," + atdRecord.chargeSchema + ",," + atdRecord.accountCurrency + ","
								+ atdRecord.maskedPan + "," + atdRecord.amountToCollect + ","
								+ atdRecord.directDebitAccount + ",D";
						
					}

					////////////////////// END OFS MAPPING //////////////////////
					
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println(finalArray);
		String ofsToQueue = "MDI.CALCULATE.AVAILABLE.BALANCE,,INPUTT/123456,," + finalArray;
		exchange.getIn().setBody(ofsToQueue);

	}

}
