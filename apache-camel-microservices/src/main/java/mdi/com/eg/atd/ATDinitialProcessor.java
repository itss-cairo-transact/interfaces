package mdi.com.eg.atd;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ATDinitialProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		String actualMsg = exchange.getIn().getBody(String.class);
		final String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		System.setProperty("fileName", camelFileName);
		System.setProperty("TotalAmount","0");
		System.setProperty("ofsResponse", "");
		String[] lines;
		lines = actualMsg.split("\\R");
		exchange.getIn().setBody(lines);
		
	}

}
