package mdi.com.eg.erp;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ERPreplyProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String actualMsg = exchange.getIn().getBody(String.class);
		
		String filename = System.getProperty("fileName");
		exchange.getIn().setHeader(Exchange.FILE_NAME, filename);
		System.out.println(filename);
		System.out.println("*************************************************");
		System.out.println(actualMsg);
	}

}
