package mdi.com.eg.erp;

import java.io.InputStream;
import java.util.Properties;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ERProute extends RouteBuilder {
	@Override
	public void configure() throws Exception  {
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String from = properties.getProperty("ERP.sourceFolder");
		String to = properties.getProperty("ERP.queue");
		String queueProvider = properties.getProperty("queueProvider");
		String replyDir = properties.getProperty("ERP.replyFolder");
		String replyQueue = properties.getProperty("ERP.replyQueue");
		
		
		
		from(from+"?move=backup/${file:onlyname}")
		.bean(ERPtransforming.class)
		.split(body())
		.setExchangePattern(ExchangePattern.InOut)
		.to(queueProvider+":"+to+"?replyTo="+replyQueue)
		.log("InOut MEP received ${body}")
		.bean(ERPreplyProcessor.class)
		.transform(body().append("\n"))
		.to(replyDir+"?fileExist=Append&charset=utf-8");
		
		
		
	}

}
