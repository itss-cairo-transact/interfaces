package mdi.com.eg.erp;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ERPtransforming implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		String actualMsg = exchange.getIn().getBody(String.class);
		final String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		System.setProperty("fileName", camelFileName);
		
		System.out.println(actualMsg);
		String[] lines;
		lines = actualMsg.split("\\R");
		exchange.getIn().setBody(lines);
	}

	

}
