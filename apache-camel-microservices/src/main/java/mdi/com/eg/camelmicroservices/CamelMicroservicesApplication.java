package mdi.com.eg.camelmicroservices;




import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;



@ComponentScan(basePackages="mdi.com.eg.atd")
@ComponentScan(basePackages="mdi.com.eg.acq")
@ComponentScan(basePackages="mdi.com.eg.erp")
@ComponentScan(basePackages="mdi.com.eg.gl")
@ImportResource({"classpath:importxml.xml"})
@SpringBootApplication
public class CamelMicroservicesApplication extends SpringBootServletInitializer
{


	public static void main(String[] args) {
		SpringApplication.run(CamelMicroservicesApplication.class, args);
	}


}
