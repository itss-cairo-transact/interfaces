package mdi.com.eg.acq;

public class ACQrecord {
	String pan;
	String amountTransaction;
	String transmitionDateTime;
	String systemTraceAuditNum;
	String timeLocalTransaction;
	String dateLocalTransaction;
	String dateSettlement;
	String acquiringInstitutionCountryCode;
	String acquiringInstitutionIdentCode;
	String retrievalReferenceNum;
	String cardAcceptorTerminalIdent;
	String cardAcceptorIdentCode;
	String cardAcceptorNameLocation;
	String currencyCode;
	String debitAccount;
	String creditAccount;
	
	public ACQrecord()
	{
		
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAmountTransaction() {
		return amountTransaction;
	}

	public void setAmountTransaction(String amountTransaction) {
		this.amountTransaction = amountTransaction;
	}

	public String getTransmitionDateTime() {
		return transmitionDateTime;
	}

	public void setTransmitionDateTime(String transmitionDateTime) {
		this.transmitionDateTime = transmitionDateTime;
	}

	public String getSystemTraceAuditNum() {
		return systemTraceAuditNum;
	}

	public void setSystemTraceAuditNum(String systemTraceAuditNum) {
		this.systemTraceAuditNum = systemTraceAuditNum;
	}

	public String getTimeLocalTransaction() {
		return timeLocalTransaction;
	}

	public void setTimeLocalTransaction(String timeLocalTransaction) {
		this.timeLocalTransaction = timeLocalTransaction;
	}

	public String getDateLocalTransaction() {
		return dateLocalTransaction;
	}

	public void setDateLocalTransaction(String dateLocalTransaction) {
		this.dateLocalTransaction = dateLocalTransaction;
	}

	public String getDateSettlement() {
		return dateSettlement;
	}

	public void setDateSettlement(String dateSettlement) {
		this.dateSettlement = dateSettlement;
	}

	public String getAcquiringInstitutionCountryCode() {
		return acquiringInstitutionCountryCode;
	}

	public void setAcquiringInstitutionCountryCode(String acquiringInstitutionCountryCode) {
		this.acquiringInstitutionCountryCode = acquiringInstitutionCountryCode;
	}

	public String getAcquiringInstitutionIdentCode() {
		return acquiringInstitutionIdentCode;
	}

	public void setAcquiringInstitutionIdentCode(String acquiringInstitutionIdentCode) {
		this.acquiringInstitutionIdentCode = acquiringInstitutionIdentCode;
	}

	public String getRetrievalReferenceNum() {
		return retrievalReferenceNum;
	}

	public void setRetrievalReferenceNum(String retrievalReferenceNum) {
		this.retrievalReferenceNum = retrievalReferenceNum;
	}

	public String getCardAcceptorTerminalIdent() {
		return cardAcceptorTerminalIdent;
	}

	public void setCardAcceptorTerminalIdent(String cardAcceptorTerminalIdent) {
		this.cardAcceptorTerminalIdent = cardAcceptorTerminalIdent;
	}

	public String getCardAcceptorIdentCode() {
		return cardAcceptorIdentCode;
	}

	public void setCardAcceptorIdentCode(String cardAcceptorIdentCode) {
		this.cardAcceptorIdentCode = cardAcceptorIdentCode;
	}

	public String getCardAcceptorNameLocation() {
		return cardAcceptorNameLocation;
	}

	public void setCardAcceptorNameLocation(String cardAcceptorNameLocation) {
		this.cardAcceptorNameLocation = cardAcceptorNameLocation;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}
	
}
