package mdi.com.eg.acq;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

public class ACQtransform implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);

		String actualMsg = exchange.getIn().getBody(String.class);
		final String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		System.setProperty("fileName", camelFileName);
		System.out.println(actualMsg);
		String[] lines;
		lines = actualMsg.split("\\R");
		int linesSize = lines.length;
		List<String> test1 = new ArrayList<>();
		String currentLine;

		try {
			if (linesSize > 0) {
				ACQrecord acqRecord = new ACQrecord();

				for (int i = 0; i < linesSize; i++) {

					List<String> lineCommaDelim = new ArrayList<String>();
					currentLine = lines[i];
					lineCommaDelim = Arrays.asList(currentLine.split(","));
					int lineCommaDelimlength = lineCommaDelim.size();
					System.out.println(lineCommaDelimlength);
//					String currentPan = lineCommaDelim.get(0);
//					currentPan = currentPan.substring(0, 4);
					acqRecord.setPan(lineCommaDelim.get(0));
					acqRecord.setAmountTransaction(lineCommaDelim.get(1));
					acqRecord.setTransmitionDateTime(lineCommaDelim.get(2));
					acqRecord.setSystemTraceAuditNum(lineCommaDelim.get(3));
					acqRecord.setTimeLocalTransaction(lineCommaDelim.get(4));
					acqRecord.setDateLocalTransaction(lineCommaDelim.get(5));
					acqRecord.setDateSettlement(lineCommaDelim.get(6));
					acqRecord.setAcquiringInstitutionCountryCode(lineCommaDelim.get(7));
					acqRecord.setAcquiringInstitutionIdentCode(lineCommaDelim.get(8));
					acqRecord.setRetrievalReferenceNum(lineCommaDelim.get(9));
					acqRecord.setCardAcceptorTerminalIdent(lineCommaDelim.get(10));
					acqRecord.setCardAcceptorIdentCode(lineCommaDelim.get(11));
					acqRecord.setCardAcceptorNameLocation(lineCommaDelim.get(12));
					acqRecord.setCurrencyCode(lineCommaDelim.get(13));
					acqRecord.setDebitAccount(lineCommaDelim.get(14));
					acqRecord.setCreditAccount(lineCommaDelim.get(15));

					////////////// START OFS MAPPING/////////////////////////
					String acqAcEntryParam = properties.getProperty("ACQ.AC_ENTRY_PARAM");
					String dataFeild = "0000";
					String contraCateg = "", plCategory = "";
					if (acqRecord.getCreditAccount().substring(0, 1).equals("5")
							|| acqRecord.getCreditAccount().substring(0, 1).equals("6"))
						plCategory = acqRecord.getCreditAccount();
					else
						contraCateg = acqRecord.getCreditAccount();

					String ofsMessage = "CSMBULK=BOOK,,//////," + acqAcEntryParam + "," + dataFeild // DATA
							+ "," + acqRecord.getPan().substring(0, 4) // DEPARTMENT
							+ "," + acqRecord.getAmountTransaction() // AMOUNT
							+ "," + acqRecord.getSystemTraceAuditNum() // THEIR.REF
							+ "," + acqRecord.dateSettlement // VALUE.DATE
							+ "," + acqRecord.getAcquiringInstitutionCountryCode() // LOCAL.REF-L.ACQ.INST.CTRY.CODE
							+ "," + acqRecord.getRetrievalReferenceNum() // TRANS.REFERENCE
							+ "," + acqRecord.getCardAcceptorTerminalIdent() // LOCAL.REF-L.CARD.ACC.TERM.ID
							+ "," + acqRecord.getCardAcceptorIdentCode() // NARRATIVE
							+ "," + acqRecord.getCardAcceptorNameLocation() // LOCAL.REF-L.CARD.ACC.NAME.LOC
							+ "," + acqRecord.getCurrencyCode() // CURRENCY
							+ "," + acqRecord.getDebitAccount() // ACCOUNT
							+ "," // PL.CATEGORY
							+ ",D" // SIGN
							+ "," + System.getProperty("fileName") != null ? System.getProperty("fileName")
									: "ACQ" // LOCAL.REF-L.FILE.NAME
											+ "," + acqRecord.getTransmitionDateTime() // LOCAL.REF-L.TRANS.DT.TIME
											+ "," + acqRecord.getTimeLocalTransaction() // LOCAL.REF-L.LOCAL.TIME
											+ "," + acqRecord.getDateLocalTransaction() // LOCAL.REF-L.LOCAL.DATE
											+ "," + acqRecord.getAcquiringInstitutionIdentCode() // LOCAL.REF-L.ACQ.INST.ID.CODE
											+ "," + acqRecord.getPan() // LOCAL.REF-L.PAN
											+ "," + acqRecord.getCreditAccount() // L.ACCT
											+ "," // CONTRA.CATEG
											+ "_" // SEPRATOR
											+ "," + dataFeild // DATA
											+ "," + acqRecord.getPan().substring(0, 4) // DEPARTMENT
											+ "," + acqRecord.getAmountTransaction() // AMOUNT
											+ "," + acqRecord.getSystemTraceAuditNum() // THEIR.REF
											+ "," + acqRecord.dateSettlement // VALUE.DATE
											+ "," + acqRecord.getAcquiringInstitutionCountryCode() // LOCAL.REF-L.ACQ.INST.CTRY.CODE
											+ "," + acqRecord.getRetrievalReferenceNum() // TRANS.REFERENCE
											+ "," + acqRecord.getCardAcceptorTerminalIdent() // LOCAL.REF-L.CARD.ACC.TERM.ID
											+ "," + acqRecord.getCardAcceptorIdentCode() // NARRATIVE
											+ "," + acqRecord.getCardAcceptorNameLocation() // LOCAL.REF-L.CARD.ACC.NAME.LOC
											+ "," + acqRecord.getCurrencyCode() // CURRENCY
											+ "," // ACCOUNT
											+ "," + plCategory // PL.CATEGORY
											+ ",C" // SIGN
											+ "," + System.getProperty("fileName") != null
													? System.getProperty("fileName")
													: "ACQ" // LOCAL.REF-L.FILE.NAME
															+ "," + acqRecord.getTransmitionDateTime() // LOCAL.REF-L.TRANS.DT.TIME
															+ "," + acqRecord.getTimeLocalTransaction() // LOCAL.REF-L.LOCAL.TIME
															+ "," + acqRecord.getDateLocalTransaction() // LOCAL.REF-L.LOCAL.DATE
															+ "," + acqRecord.getAcquiringInstitutionIdentCode() // LOCAL.REF-L.ACQ.INST.ID.CODE
															+ "," + acqRecord.getPan() // LOCAL.REF-L.PAN
															+ "," + acqRecord.getDebitAccount() // L.ACCT
															+ "," + contraCateg; // CONTRA.CATEG
					System.out.println(ofsMessage);
					////////////// END OFS MAPPING///////////////////////////
					test1.add(ofsMessage);
				}
				exchange.getIn().setBody(test1);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
