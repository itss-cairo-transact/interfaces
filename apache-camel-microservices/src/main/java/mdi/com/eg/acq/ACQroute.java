package mdi.com.eg.acq;

import java.io.InputStream;
import java.util.Properties;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class ACQroute  extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		properties.load(inputStream);
		String from = properties.getProperty("ACQ.sourceFolder");
		String to = properties.getProperty("ACQ.queue");
		String queueProvider = properties.getProperty("queueProvider");
		String replyDir = properties.getProperty("ACQ.replyFolder");
		String replyQueue = properties.getProperty("ACQ.replyQueue");

		
		from(from+"?move=backup/${file:onlyname}")
		.bean(ACQtransform.class)
		.split(body())
		.setExchangePattern(ExchangePattern.InOut)
		.to(queueProvider+":"+to+"?replyTo="+replyQueue)
		.log("InOut MEP received ${body}")
		.bean(ACQreplyProcessor.class)
		.transform(body().append("\n"))
		.to(replyDir+"?fileExist=Append&charset=utf-8");
	}

}
