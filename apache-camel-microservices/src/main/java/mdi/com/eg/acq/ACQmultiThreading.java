package mdi.com.eg.acq;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ACQmultiThreading implements Runnable {
	String[] lines;
	List<String> test1 = new ArrayList<>();
	
	public ACQmultiThreading(String[] lines) {
		this.lines = lines;
	}
	public List<String> getFinalArray() {
		return test1;
	}
	@Override
	public void run() {
		ACQrecord acqRecord = new ACQrecord();
		String currentLine;
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("application.properties");
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int linesSize = lines.length;
		for (int i = 0; i < linesSize; i++) {
			
			List<String> lineCommaDelim = new ArrayList<String>();
			currentLine = lines[i];
			lineCommaDelim= Arrays.asList(currentLine.split(","));
			int lineCommaDelimlength = lineCommaDelim.size();
			System.out.println(lineCommaDelimlength);
			String currentPan = lineCommaDelim.get(0);
			currentPan = currentPan.substring(0,4);
			acqRecord.setPan(currentPan);
			acqRecord.setAmountTransaction(lineCommaDelim.get(1));
			acqRecord.setTransmitionDateTime(lineCommaDelim.get(2));
			acqRecord.setSystemTraceAuditNum(lineCommaDelim.get(3));
			acqRecord.setTimeLocalTransaction(lineCommaDelim.get(4));
			acqRecord.setDateLocalTransaction(lineCommaDelim.get(5));
			acqRecord.setDateSettlement(lineCommaDelim.get(6));
			acqRecord.setAcquiringInstitutionCountryCode(lineCommaDelim.get(7));
			acqRecord.setAcquiringInstitutionIdentCode(lineCommaDelim.get(8));
			acqRecord.setRetrievalReferenceNum(lineCommaDelim.get(9));
			acqRecord.setCardAcceptorTerminalIdent(lineCommaDelim.get(10));
			acqRecord.setCardAcceptorIdentCode(lineCommaDelim.get(11));
			acqRecord.setCardAcceptorNameLocation(lineCommaDelim.get(12));
			acqRecord.setCurrencyCode(lineCommaDelim.get(13));
			acqRecord.setDebitAccount(lineCommaDelim.get(14));
			acqRecord.setCreditAccount(lineCommaDelim.get(15));
			
			//////////////START OFS MAPPING/////////////////////////
			String acqAcEntryParam = properties.getProperty("ACQ.AC_ENTRY_PARAM");
			String ofsMessage = "CSMBULK=BOOK,,//////,"+acqAcEntryParam+","+acqRecord.getPan()+","+acqRecord.getPan()+","+acqRecord.getAmountTransaction()
			+","+acqRecord.getSystemTraceAuditNum()+","+acqRecord.dateSettlement+","+acqRecord.getAcquiringInstitutionCountryCode()+","+acqRecord.getRetrievalReferenceNum()
			+","+acqRecord.getCardAcceptorTerminalIdent()+","+acqRecord.getCardAcceptorIdentCode()+","+acqRecord.getCardAcceptorNameLocation()
			+","+acqRecord.getCurrencyCode()+","+acqRecord.getDebitAccount()+",,D_"+acqRecord.getPan()+","+acqRecord.getPan()+","+acqRecord.getAmountTransaction()
			+","+acqRecord.getSystemTraceAuditNum()+","+acqRecord.dateSettlement+","+acqRecord.getAcquiringInstitutionCountryCode()+","+acqRecord.getRetrievalReferenceNum()
			+","+acqRecord.getCardAcceptorTerminalIdent()+","+acqRecord.getCardAcceptorIdentCode()+","+acqRecord.getCardAcceptorNameLocation()
			+","+acqRecord.getCurrencyCode()+",,"+acqRecord.getCreditAccount()+",C";
			System.out.println(ofsMessage);
			//////////////END OFS MAPPING///////////////////////////
			test1.add(ofsMessage);
		}
		
	}

}
